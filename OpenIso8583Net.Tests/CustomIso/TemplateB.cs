﻿using OpenIso8583Net.FieldValidator;
using OpenIso8583Net.Formatter;
using OpenIso8583Net.LengthFormatters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenIso8583Net.Tests.CustomIso
{
    public static class TemplateB
    {
        public static Template GetPack()
        {
            var template = new Template
                {
                    { Iso8583.Bit._002_PAN, FieldDescriptor.BcdVar(2, 19, Formatters.Bcd) },
                    { Iso8583.Bit._003_PROC_CODE, FieldDescriptor.BinaryFixed(3) },
                    { Iso8583.Bit._004_TRAN_AMOUNT, FieldDescriptor.BcdFixed(6) },
                    { Iso8583.Bit._005_SETTLE_AMOUNT, FieldDescriptor.AsciiFixed(12, FieldValidators.N) },
                    { Iso8583.Bit._011_SYS_TRACE_AUDIT_NUM, FieldDescriptor.BcdFixed(3) },
                    { Iso8583.Bit._012_LOCAL_TRAN_TIME, FieldDescriptor.BcdFixed(3) },
                    { Iso8583.Bit._013_LOCAL_TRAN_DATE, FieldDescriptor.BcdFixed(2) },
                    { Iso8583.Bit._014_EXPIRATION_DATE, FieldDescriptor.BcdFixed(2) },
                    { Iso8583.Bit._022_POS_ENTRY_MODE, FieldDescriptor.BcdFixed(2) },
                    { Iso8583.Bit._023_CARD_SEQUENCE_NUM, FieldDescriptor.BcdFixed(2) },
                    { 24, FieldDescriptor.BcdFixed(2) },
                    { Iso8583.Bit._025_POS_CONDITION_CODE, FieldDescriptor.BcdFixed(1) },
                    { Iso8583.Bit._032_ACQUIRING_INST_ID_CODE, FieldDescriptor.Create(new VariableLengthFormatter(2, 99, Formatters.Bcd), FieldValidators.N, Formatters.Bcd)},
                    { Iso8583.Bit._035_TRACK_2_DATA, FieldDescriptor.Create(new VariableLengthFormatter(1, 50, Formatters.Bcd), FieldValidators.Track2, Formatters.Z) },
                    { Iso8583.Bit._037_RETRIEVAL_REF_NUM, FieldDescriptor.AsciiFixed(12, FieldValidators.An) },
                    { Iso8583.Bit._038_AUTH_ID_RESPONSE, FieldDescriptor.AsciiFixed(6, FieldValidators.An) },
                    { Iso8583.Bit._039_RESPONSE_CODE, FieldDescriptor.AsciiFixed(2, FieldValidators.An) },
                    { Iso8583.Bit._040_SERVICE_RESTRICTION_CODE, FieldDescriptor.AsciiFixed(8, FieldValidators.Ans) },
                    { Iso8583.Bit._041_CARD_ACCEPTOR_TERMINAL_ID, FieldDescriptor.AsciiFixed(8, FieldValidators.Ans) },
                    { Iso8583.Bit._042_CARD_ACCEPTOR_ID_CODE, FieldDescriptor.AsciiFixed(12, FieldValidators.Ans) },
                    { Iso8583.Bit._043_CARD_ACCEPTOR_NAME_LOCATION, FieldDescriptor.AsciiFixed(40, FieldValidators.Ans) },
                    { Iso8583.Bit._045_TRACK_1_DATA, FieldDescriptor.AsciiVar(2, 76, FieldValidators.Ans) },
                    { 46, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Ans, Formatters.Ascii) },
                    { 47, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Ans, Formatters.Ascii) },
                    { Iso8583.Bit._048_ADDITIONAL_DATA, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Ans, Formatters.Ascii) },
                    { Iso8583.Bit._049_TRAN_CURRENCY_CODE, FieldDescriptor.AsciiFixed(3, FieldValidators.AorN) },
                    { Iso8583.Bit._052_PIN_DATA, FieldDescriptor.BinaryFixed(8) },
                    { Iso8583.Bit._054_ADDITIONAL_AMOUNTS, FieldDescriptor.AsciiVar(3, 120, FieldValidators.An) },
                    { 55, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Hex, Formatters.Binary) },
                    { Iso8583.Bit._056_MESSAGE_REASON_CODE, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Ans, Formatters.Ascii) },
                    { Iso8583.Bit._057_AUTHORISATION_LIFE_CYCLE, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Hex, Formatters.Binary) },
                    { Iso8583.Bit._058_AUTHORISING_AGENT_INSTITUTION, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Hex, Formatters.Binary) },
                    { 60, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Hex, Formatters.Binary) },
                    { 61, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Ans, Formatters.Ascii) },
                    { 62, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.AlphaNumeric, Formatters.Ascii) },
                    { 63, FieldDescriptor.Create(new VariableLengthFormatter(4, 999, Formatters.Bcd), FieldValidators.Hex, Formatters.Binary) }
                };
            template.MsgTypeFormatter = new BinaryFormatter();
            
            return template;
        }
    }
}
