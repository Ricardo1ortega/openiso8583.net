﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenIso8583Net.Tests.CustomIso
{
    [TestClass]
    public class TemplateBTests
    {
        /// <summary>
        /// A test for PackTest
        /// </summary>
        [TestMethod]
        public void PackTest()
        {
            var template = TemplateB.GetPack();

            var iso = new Iso8583(template)
            {
                MessageType = Iso8583.MsgType._0800_NWRK_MNG_REQ,
                [Iso8583.Bit._003_PROC_CODE] = "990000",
                [24] = "0151",
                [Iso8583.Bit._041_CARD_ACCEPTOR_TERMINAL_ID] = "14588480",
                [60] = "050000100000",
            };

            var actual = iso.ToMsg();
            var a = actual.ToHex();
            var expected =
                "08002000010000800010990000015131343538383438300006050000100000"
                    .ToByteArray();
            CollectionAssert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for PackLogon
        /// </summary>
        [TestMethod]
        public void PackLogon()
        {
            var template = TemplateB.GetPack();

            var iso = new Iso8583(template)
            {
                MessageType = Iso8583.MsgType._0800_NWRK_MNG_REQ,
                [Iso8583.Bit._003_PROC_CODE] = "990001",
                [24] = "0003",
                [Iso8583.Bit._041_CARD_ACCEPTOR_TERMINAL_ID] = "20981026",
                [Iso8583.Bit._056_MESSAGE_REASON_CODE] = "FB45863E6B12F957156DD422CF028CAF",
            };

            var actual = iso.ToMsg();
            var a = actual.ToHex();
            var expected =
                "080020000100008001009900010003323039383130323600324642343538363345364231324639353731353644443432324346303238434146"
                    .ToByteArray();
            CollectionAssert.AreEqual(expected, actual);
        }

        /// <summary>
        /// A test for Sale
        /// </summary>
        [TestMethod]
        public void PackSale()
        {
            var template = TemplateB.GetPack();
            var iso = new Iso8583(template)
            {
                MessageType = Iso8583.MsgType._0200_TRAN_REQ,
                [Iso8583.Bit._003_PROC_CODE] = "004000",
                [Iso8583.Bit._004_TRAN_AMOUNT] = "000000010000".PadLeft(12, '0'),//tx.AmountAsString,
                [Iso8583.Bit._011_SYS_TRACE_AUDIT_NUM] = "120537".PadLeft(6, '0'),
                [Iso8583.Bit._012_LOCAL_TRAN_TIME] = "120537",
                [Iso8583.Bit._013_LOCAL_TRAN_DATE] = "0425",
                [Iso8583.Bit._022_POS_ENTRY_MODE] = "051".PadLeft(4, '0'),
                [Iso8583.Bit._023_CARD_SEQUENCE_NUM] = "000".PadLeft(4, '0'),
                [24] = "151".PadLeft(4, '0'),
                [Iso8583.Bit._025_POS_CONDITION_CODE] = "00",
                [Iso8583.Bit._035_TRACK_2_DATA] = "5401323112932116D23012013950782900000",
                [Iso8583.Bit._037_RETRIEVAL_REF_NUM] = "811579810001",
                [Iso8583.Bit._038_AUTH_ID_RESPONSE] = "000000",
                [Iso8583.Bit._039_RESPONSE_CODE] = "97",
                [Iso8583.Bit._041_CARD_ACCEPTOR_TERMINAL_ID] = "39867981",
                [Iso8583.Bit._042_CARD_ACCEPTOR_ID_CODE] = "000001047190".PadRight(12, ' '),
                [47] = "  COMS0203080005A082541",
                [Iso8583.Bit._048_ADDITIONAL_DATA] = "R41161112000000000001",
                [55] = "5F2A0209285F340100820218008407A00000000410104F07A0000000041010950580000080009A031804259C01009F02060000000100009F03060000000000009F090200029F10120210A00001220000000000000000000000FF9F1A0208629F1E0835413038323534319F1F01009F260830F6DF50B8BCD1079F2701809F3303E0F0C89F34031E03009F3501229F360200899F3704FD75A54B9F530100",
                [61] = "00000002005",
                [62] = "000001",
            };

            var actual = iso.ToMsg();

            var expected =
                "0200303807802EC3020C004000000000010000120537120537042500510000015100375401323112932116D2301201395078290000003831313537393831303030313030303030303937333938363739383130303030303130343731393000232020434F4D533032303330383030303541303832353431002152343131363131313230303030303030303030303101575F2A0209285F340100820218008407A00000000410104F07A0000000041010950580000080009A031804259C01009F02060000000100009F03060000000000009F090200029F10120210A00001220000000000000000000000FF9F1A0208629F1E0835413038323534319F1F01009F260830F6DF50B8BCD1079F2701809F3303E0F0C89F34031E03009F3501229F360200899F3704FD75A54B9F530100001130303030303030323030350006303030303031"
                    .ToByteArray();

            var a = actual.ToHex();
            var b = Utils.DebugPrint(expected);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
